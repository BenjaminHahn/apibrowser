package de.hahn.apibrowser.views;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.control.ListView;

import java.util.Map;

/**
 * A {@link ListView} that displays displays the Outline of an {@link EngineTab}.
 */
public class OutlineView extends ListView<String> {

    public void displayOutline(EngineTab tab) {
        Task<Map<String, String>> calcOutline = new Task<Map<String, String>>() {

            @Override
            protected Map<String, String> call() throws Exception {
                //calculate the outline of the Page
                return tab.getOutline();
            }

            @Override
            protected void succeeded() {
                Map<String, String> outline = getValue();
                ObservableList<String> outlineKeys = FXCollections
                        .observableArrayList(outline.keySet());
                // show the outline
                setItems(outlineKeys);

                // move the page to the right method
                setOnMouseClicked(event -> {
                    String selectedItem = getSelectionModel().getSelectedItem();
                    String methodName = outline.get(selectedItem);
                    tab.movePageTo(methodName);
                });
            }
        };
        new Thread(calcOutline).start();
    }

    public void clear() {
        getSelectionModel().getSelectedItems().clear();
    }
}
