package de.hahn.apibrowser.views;

import de.hahn.apibrowser.Controller;
import de.hahn.apibrowser.Page;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * A SearchField that filters a {@link ListView} Created by post_000 on
 * 26.12.2014.
 */
public class SearchField extends TextField {
    enum Prefix {
        EXACT, CONTAINS, STARTS_WITH
    }

    private ObservableList<Page> completeList =
            FXCollections.observableArrayList();
    ListView<Page> listView;

    public SearchField() {
        textProperty().addListener((observable, oldValue, newValue) -> {
            filterList(newValue);
        });

        // on ENTER open all matching pages
        setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                String searchWord = getText();
                for (Page page : listView.getItems()) {
                    // remove possible prefixes
                    if (hasExactPrefix(searchWord) ||
                            hasStartsWithPrefix(searchWord)) {
                        searchWord = removePrefix(searchWord);
                    }
                    // load a page
                    String pageTitle = page.getName().toLowerCase();
                    if (pageTitle.equalsIgnoreCase(searchWord))
                        Controller.getReference().loadBrowserPage(page);
                }
            }
        });
    }

    public SearchField setListView(ListView<Page> listView) {
        this.completeList.addAll(listView.getItems());
        this.listView = listView;
        return this;
    }

    /**
     * filter the content of the ListView
     *
     * @param searchWord the filter rules
     */
    private void filterList(String searchWord) {

        searchWord = searchWord.toLowerCase();
        // when the searchWord is empty set the complete list
        if (searchWord.isEmpty()) {
            listView.setItems(completeList);
            return;
        }

        Predicate<Page> filter;
        switch (findPrefix(searchWord)) {
            case EXACT:
                filter = equalsSearchWord(searchWord);
                break;
            case STARTS_WITH:
                filter = startsWithSearchWord(searchWord);
                break;
            case CONTAINS:
                filter = containsSearchWord(searchWord);
                break;
            default:
                filter = containsSearchWord(searchWord);
        }

        List<Page> filteredList = completeList.stream()
                .filter(filter)
                .collect(Collectors.toCollection(ArrayList<Page>::new));

        listView.setItems(FXCollections.observableArrayList(filteredList));
    }

    private Predicate<Page> equalsSearchWord(final String searchWord) {
        final String search = removePrefix(searchWord);
        return p -> p.getName().toLowerCase().equals(search);
    }

    private Predicate<Page> startsWithSearchWord(final String searchWord) {
        final String search = removePrefix(searchWord);
        return p -> p.getName().toLowerCase().startsWith(search);
    }

    private Predicate<Page> containsSearchWord(final String searchWord) {
        final String search = removePrefix(searchWord);
        return p -> p.getName().toLowerCase().contains(search);
    }

    private Prefix findPrefix(String searchWord) {
        if (hasExactPrefix(searchWord)) return Prefix.EXACT;
        else if (hasStartsWithPrefix(searchWord)) return Prefix.STARTS_WITH;
        else return Prefix.CONTAINS;
    }

    private boolean hasExactPrefix(String s) {
        return s.startsWith("*");
    }

    private boolean hasStartsWithPrefix(String s) {
        return s.startsWith("?");
    }

    private String removePrefix(String s) {
        return s.substring(1);
    }


}
