package de.hahn.apibrowser.views;

import de.hahn.apibrowser.Api;
import de.hahn.apibrowser.Controller;
import de.hahn.apibrowser.Page;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.control.Accordion;
import javafx.scene.control.ListView;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.BorderPane;

import java.util.Collection;

/**
 * A Accordion that creates a {@link TitledPane} and a {@link SearchField} for every added Api.
 */
public class ApiAccordion extends Accordion {

    /**
     * @param apis create a {@link TitledPane} for each api.
     */
    public void addApis(Collection<Api> apis) {
        apis.forEach(this::addApi);
    }

    /**
     * @param api create a {@link TitledPane} for the api.
     */
    public void addApi(Api api) {
        // the list displays the api content
        ListView<Page> pagesListView = new ListView<>();
        pagesListView.setItems(FXCollections.observableArrayList(api.getPages()));

        // add to layout
        SearchField searchField = new SearchField().setListView(pagesListView);
        BorderPane pane = new BorderPane();
        pane.setTop(searchField);
        pane.setCenter(pagesListView);
        BorderPane.setMargin(searchField, new Insets(0, 0, 12, 0));

        TitledPane menuItem = new TitledPane(api.getName(), pane);
        pagesListView.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case ENTER: {
                    openSelectedPage(pagesListView);
                    break;
                }
                // remove focus of the SearchField ESC
                case ESCAPE:
                    menuItem.requestFocus();
            }
        });
        pagesListView.setOnMouseClicked(event -> {
            openSelectedPage(pagesListView);
        });
        getPanes().add(menuItem);
    }

    /**
     * Open the selected page on the ListView.
     *
     * @param listview the {@link ListView} with the selected item.
     */
    private void openSelectedPage(ListView<Page> listview) {
        Page selectedPage = listview.getSelectionModel().getSelectedItem();
        Controller.getReference().loadBrowserPage(selectedPage);
    }
}
