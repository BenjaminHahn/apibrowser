package de.hahn.apibrowser.views;

import de.hahn.apibrowser.Page;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.TreeMap;

public class EngineTab extends Tab {

    Logger logger = Logger.getLogger(getClass().getName());

    @FXML
    private WebView browser;
    private WebEngine engine;
    private Path path;
    private OutlineView outline;

    public EngineTab(OutlineView outline) {
        loadLayout();

        engine = browser.getEngine();
        this.outline = outline;
        addListener();
    }

    private void loadLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("engine_tab.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param path load the file under the path in the {@link EngineTab}.
     */
    public void loadPage(Path path) {
        this.path = path;
        logger.debug("load " + path.toUri().toString());
        engine.load(path.toUri().toString());
    }

    /**
     * Move to a method on the displayed {@link Page}.
     *
     * @param method the name of the method
     */
    public void movePageTo(String method) {
        String uri = path.toUri() + "#" + method;
        logger.debug("move page to: " + uri);
        engine.load(uri);
    }

    private void addListener() {
        // set the tab title when finished loading
        engine.getLoadWorker().stateProperty().addListener((ov, oldState, newState) -> {
            logger.debug("loading state: " + newState);
            if (newState == Worker.State.SUCCEEDED) {
                // set tab title
                setText(removeBetweenBraces(engine.getTitle()));
            }
        });

        // display outline when tab is selected
        selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!oldValue && newValue) outline.displayOutline(this);
        });

        // update this EngineTab when a link was clicked
        engine.locationProperty().addListener((observable, oldValue, newValue) -> {
            path = Paths.get(removeFilePrefix(newValue));
            outline.displayOutline(this);
        });
    }

    /**
     * Calculates the outline of the displayed page.
     *
     * @return the outline as name and link mapping
     */
    public Map<String, String> getOutline() {
        Map<String, String> outline = new TreeMap<>();
        Document doc = null;
        try {
            doc = Jsoup.parse(path.toFile(), "UTF-8");
            Elements allElements = doc.getAllElements();
            Elements links = allElements
                    .select("a[href~=" + "/" + getPath().getFileName() + "#.*]");
            for (Element link : links) {
                outline.put(link.text(), extractSublink(link.attr("href")));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outline;
    }

    private String extractSublink(String s) {
        return StringUtils.substringAfter(s, "#");
    }

    private String removeFilePrefix(String s) {
        return StringUtils.stripStart(s, "file:/");
    }

    public Path getPath() {
        return path;
    }

    /**
     * remove round braces and everything in between
     *
     * @param s source String
     * @return the cleared String
     */
    private String removeBetweenBraces(String s) {
        return StringUtils.removePattern(s, "\\([a-zA-Z0-9 \\.]*\\)");
    }
}
