package de.hahn.apibrowser.settings;

import de.hahn.apibrowser.Api;
import de.hahn.apibrowser.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;

/**
 * Read HTML Pages from the file system. This class loads the {@link Page}s for a {@link Api}.
 */
final class FileScanner {

    private static final Logger logger = Logger.getLogger(FileScanner.class.getName());

    private FileScanner() {

    }

    /**
     * Reads the HTML pages from the file system.
     *
     * @param api contains the directory from which the pages will be fetched.
     * @return all found {@link Page}s.
     */
    static Collection<Page> readPages(final Api api) {
        logger.debug("STARTED fetching files for " + api.getName());

        Collection<Path> files = new ArrayList<>();

        // collect files down a base dir
        try {
            Files.walkFileTree(api.getPath(), new GatherPathsVisitor(files, api.getBlacklist()));
        } catch (IOException e) {
            logger.error("no files found under folder: " + api.getPath());
        }

        Collection<Page> pages = new TreeSet<>();
        for (Path path : files) {
            Page page = new Page(path, api);
            pages.add(page);
        }

        logger.debug("FINISHED fetching files for " + api.getName());
        return pages;
    }

    /**
     * The FileVisitor searches down a base dir and saves all found HTML files.
     */
    static class GatherPathsVisitor extends SimpleFileVisitor<Path> {

        private Collection<Path> files;
        private Collection<String> blacklist;

        GatherPathsVisitor(Collection<Path> files, Collection<String> blacklist) {
            this.files = files;
            this.blacklist = blacklist;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            // include only html files and check the blacklist and remove
            // all file with completely lowercase
            if (isHtmlFile(file) &&
                    !onBlacklist(file, blacklist) &&
                    !isFileNameAllLowerCase(file)) {
                files.add(file);
            }
            return FileVisitResult.CONTINUE;
        }
    }

    /**
     * Test if the given path ends with .html
     *
     * @param file the file under test
     * @return the boolean result
     */
    private static boolean isHtmlFile(Path file) {
        return file.toString().endsWith(".html");
    }

    /**
     * Check the file against a blacklist
     *
     * @param file      the file under check
     * @param blacklist the blacklist contains forbidden words
     * @return does the file path contain any of the forbidden words
     */
    private static boolean onBlacklist(Path file, Collection<String> blacklist) {
        for (String s : blacklist) {
            if (file.toString().contains(s)) return true;
        }
        return false;
    }

    /**
     * Check if a filename is completely lowercase
     *
     * @param file the file to check
     * @return the result
     */
    private static boolean isFileNameAllLowerCase(Path file) {
        // clear all non characters
        String cleared = StringUtils
                .replacePattern(file.getFileName().toString(), "[^a-zA-Z]", StringUtils.EMPTY);
        return StringUtils.isAllLowerCase(cleared);
    }
}
