package de.hahn.apibrowser.settings;

import de.hahn.apibrowser.Api;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.concurrent.RecursiveAction;

/**
 * Takes a Collection of Apis and calculates parallel the Pages. Created by
 * post_000 on 02.08.2014.
 */
public class RecursiveCalculatePages extends RecursiveAction {

    private static final Logger logger = Logger.getLogger(
            RecursiveCalculatePages.class);

    private List<Api> apis;

    private int start;

    private int end;

    public RecursiveCalculatePages(List<Api> apis, int s, int e) {
        this.apis = apis;
        start = s;
        end = e;
        assert start <= end : "start muss kleiner oder gleich als ende sein";
        logger.debug("starting task " + start + " " + end);
    }

    @Override
    protected void compute() {
        // when only one api is in range calculate the pages
        if (start == end) {
            // do sequential
            Api api = apis.get(start);
            // calculate the pages of this api and add them
            api.setPages(FileScanner.readPages(api));
        } else {
            // when more than on start a new task with just one api and start
            // another tread for the rest
            // do in some kind of divide and conquer
            invokeAll(new RecursiveCalculatePages(apis, start, start),
                    new RecursiveCalculatePages(apis, start + 1, end));
        }
    }
}
