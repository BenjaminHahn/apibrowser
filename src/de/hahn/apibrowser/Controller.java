package de.hahn.apibrowser;

import de.hahn.apibrowser.settings.Settings;
import de.hahn.apibrowser.views.ApiAccordion;
import de.hahn.apibrowser.views.EngineTab;
import de.hahn.apibrowser.views.OutlineView;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TabPane;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private static Logger logger = Logger.getLogger(Controller.class.getName());

    @FXML
    private ApiAccordion apiAccordion;

    @FXML
    private OutlineView outline;

    @FXML
    private TabPane tabPane;

    @FXML
    private VBox mainWindow;

    private static Controller self;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        calculateApis();
        self = this;
        mainWindow.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                // close the selected Tab with ALT + F4
                if (pressedControlF4(event)) {
                    int selectedIndex = tabPane.getSelectionModel().getSelectedIndex();
                    if (selectedIndex != -1) {
                        tabPane.getTabs().remove(selectedIndex);
                    }
                }
                // switch tabs via CONTROL + NUMBER(1-9)
                else if (pressedControlNumber(event)) {
                    switch (event.getCode()) {
                        case DIGIT1:
                            switchTab(0);
                            break;
                        case DIGIT2:
                            switchTab(1);
                            break;
                        case DIGIT3:
                            switchTab(2);
                            break;
                        case DIGIT4:
                            switchTab(3);
                            break;
                        case DIGIT5:
                            switchTab(4);
                            break;
                        case DIGIT6:
                            switchTab(5);
                            break;
                        case DIGIT7:
                            switchTab(6);
                            break;
                        case DIGIT8:
                            switchTab(7);
                            break;
                        case DIGIT9:
                            switchTab(8);
                            break;
                    }
                } else if (pressedFKey(event)) {
                    switch (event.getCode()) {
                        case F1:
                            switchAccordionPane(0);
                            break;
                        case F2:
                            switchAccordionPane(1);
                            break;
                        case F3:
                            switchAccordionPane(2);
                            break;
                        case F4:
                            switchAccordionPane(3);
                            break;
                        case F5:
                            switchAccordionPane(4);
                            break;
                        case F6:
                            switchAccordionPane(5);
                            break;
                        case F7:
                            switchAccordionPane(6);
                            break;
                        case F8:
                            switchAccordionPane(7);
                            break;
                        case F9:
                            switchAccordionPane(8);
                            break;
                        case F10:
                            switchAccordionPane(9);
                            break;
                    }
                }
                event.consume();
            }
        });
    }

    private boolean pressedControlF4(KeyEvent event) {
        return event.isControlDown() && event.getCode() == KeyCode.F4;
    }

    private boolean pressedControlNumber(KeyEvent event) {
        return event.isControlDown() && event.getCode().isDigitKey();
    }

    private boolean pressedFKey(KeyEvent event) {
        return event.getCode().isFunctionKey();
    }

    private void switchTab(int index) {
        tabPane.getSelectionModel().select(index);
    }

    private void switchAccordionPane(int index) {
        ObservableList<TitledPane> panes = apiAccordion.getPanes();
        // check if index exceeds available panes
        if (panes.size() > index) {
            TitledPane titledPane = panes.get(index);
            apiAccordion.setExpandedPane(titledPane);
            titledPane.requestFocus();
        }
    }

    public void calculateApis() {
        Settings settings = Settings.loadFile().loadAllPages();
        Collection<Api> apis = settings.getApis();
        apiAccordion.addApis(apis);
    }

    /**
     * Display a page in the TabPane
     * @param page the page which will be displayed
     */
    public void loadBrowserPage(final Page page) {
        EngineTab tab = new EngineTab(outline);
        tab.loadPage(page.getPath());
        tabPane.getTabs().add(tab);
        tabPane.getSelectionModel().select(tab);
    }

    public static Controller getReference() {
        return self;
    }
}
