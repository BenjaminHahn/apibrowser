package de.hahn.apibrowser;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.nio.file.Path;
import java.security.InvalidParameterException;
import java.util.regex.Pattern;

/**
 * this class is the representation of a html file on the file system. It's uses to store data about
 * this file
 *
 * @author post_000
 */
public class Page implements Comparable<Page> {

    private static final Logger logger = Logger.getLogger(Page.class.getName());

    /**
     * same of the class or interface
     */
    private final String name;

    /**
     * path to the html file starting from the api path
     */
    private final Path path;

    /**
     * the package of the file
     */
    private String classPackage;

    /**
     * the coresponding api of this key
     */
    private final Api api;

    public Page(Path path, Api api) {
        if (!StringUtils.endsWith(path.toString(), ".html"))
            throw new InvalidParameterException("Page is no .html file: " + path);
        this.name = path.getFileName().toString().replace(".html", "");
        this.path = path;
        this.api = api;

        calculatePackage();
    }

    public String getName() {
        return name;
    }

    public Path getPath() {
        return path;
    }

    public String getPackage() {
        return classPackage;
    }

    public Api getApi() {
        return api;
    }

    /**
     * extract the package from the file path
     */
    private void calculatePackage() {
        String s = path.toAbsolutePath().toString();
        s = StringUtils.remove(s, api.getPath().toString());
        s = StringUtils.remove(s, name + ".html");
        s = StringUtils.replace(s, "\\", ".");
        s = StringUtils.strip(s, ".");
        classPackage = s;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Page other) {
        boolean startsWithCharThis = Pattern.matches("[a-zA-Z].*", getName());
        boolean startsWithCharOther = Pattern.matches("[a-zA-Z].*", other.getName());

        // when the other does not start with an char then this should be above
        if (startsWithCharThis && !startsWithCharOther) {
            return -1;
        } else if (!startsWithCharThis && startsWithCharOther) {
            // other way around
            return 1;
        }

        int name = getName().toLowerCase().compareTo(other.getName().toLowerCase());
        if (name != 0) {
            // when names are not the same return here the natural roder
            return name;
        }
        // else check the path
        int path = getPath().compareTo(other.getPath());
        if (path != 0) {
            // when paths are not the same return here the natural roder
            return path;
        }

        // the method should never come so far.. but when then the keys are the
        // same
        return 0;
    }
}
