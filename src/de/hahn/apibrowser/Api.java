package de.hahn.apibrowser;

import de.hahn.apibrowser.settings.Settings;
import org.apache.log4j.Logger;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * store information about an api
 *
 * @author post_000
 */
public class Api {

    private static final Logger logger = Logger.getLogger(Api.class.getName());
    /**
     * name of the api for example "Java" or "Android". also displayed in the application itself
     */
    private String name;

    /**
     * the directory on the file system. something like "C:\OfflineApi\..." defined in the settings
     * file, located in the same directory as the application-jar. Is only uses once when the
     * classes and interfaces of this api are fetched. Basically it's the base directory
     */
    private Path directory;

    /**
     * holds the packages of the api
     */
    private Collection<String> packages;

    /**
     * Holds all the pages to this api. Normally received using the FileScanner Class
     */
    private Collection<Page> pages = new ArrayList<>();

    /**
     * a collection containing keywords which should be ignored
     */
    private Collection<String> blacklist = new HashSet<>();

    private static List<String> defaultBlacklist = Arrays.asList("class-use");

    public Api(String name, String directory) {
        super();
        this.name = name;
        this.directory = Paths.get(directory);
        blacklist.addAll(defaultBlacklist);
    }

    public String getName() {
        return name;
    }

    public Path getPath() {
        return directory;
    }

    public Collection<Page> getPages() {
        assert pages != null : "Pages of " + getName() + " are not loaded";
        return pages;
    }

    /**
     * @param name name of the requested Page
     * @return the found Page or an empty one
     */
    public Page findPage(String name) {
        for (Page page : pages) {
            if (page.getName().equalsIgnoreCase(name)) return page;
        }
        logger.debug("no page found: " + name);
        return new Page(Paths.get("page not found"), this);
    }

    /**
     * set the {@link Page} of the of this api
     *
     * @param pages the collection containing the pages
     * @return this api for method chaining
     */
    public Api setPages(Collection<Page> pages) {
        this.pages = pages;
        calculatePackages();
        return this;
    }

    private void calculatePackages() {
        packages = new TreeSet<String>();
        for (Page page : pages) {
            packages.add(page.getPackage());
        }
    }

    public Collection<String> getPackages() {
        return packages;
    }

    public Api load() {
        setPages(Settings.loadFile().loadApiPages(this));
        return this;
    }

    @Override
    public String toString() {
        return name;
    }


    public Collection<String> getBlacklist() {
        return blacklist;
    }

    /**
     * add a keyword to the blacklist
     */
    public void addToBlacklist(String word) {
        blacklist.add(word);
    }

    /**
     * add a hole bunch of keywords to the blacklist
     *
     * @param collection the collection contains all keywords which should be ignored
     */
    public void addAllToBlacklist(Collection<String> collection) {
        blacklist.addAll(collection);
    }
}
