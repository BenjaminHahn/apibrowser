package de.hahn.apibrowser;

import de.hahn.apibrowser.settings.Settings;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestSettings {

    @Test
    public void testSingleton() {
        Settings sFile1 = Settings.loadFile();
        Settings sFile2 = Settings.loadFile();

        assertThat(sFile1).isSameAs(sFile2);
    }

    @Test
    public void testLoad() {
        Settings settings = Settings.loadFile();
        assertThat(settings.getApis()).isNotEmpty();
    }

    @Test
    public void testPagesLoaded() {
        Settings settings = Settings.loadFile();
        Api java8 = settings.getApiByName("Java8");
        assertThat(java8.getPages()).isNotEmpty();
    }

    @Test
    public void testPageByName() {
        Settings settings = Settings.loadFile();
        Api java8 = settings.getApiByName("JavaFX8");
        assertThat(java8.getName()).isEqualTo("JavaFX8");
    }
}
