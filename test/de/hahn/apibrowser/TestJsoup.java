package de.hahn.apibrowser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by post_000 on 27.12.2014.
 */
public class TestJsoup {

    @Test
    public void example() {
        try {
            //File input = new File(
            //"D:\\Programmieren\\Binarys\\jsoup\\jsoup-1.8.1-javadoc\\org\\jsoup\\Connection.html");
            //String classPackage = "org/jsoup/";
            //String name = "Connection.html";
            File input = new File(
                    "D:\\Programmieren\\Offline API\\java8\\api\\java\\lang\\String.html");
            String classPackage = "java/lang/";
            String name = "String.html";

            Document doc = Jsoup.parse(input, "UTF-8");
            Elements elements = doc.getAllElements();
            Elements content = elements.select("a[href~=" + classPackage + name + ".*#.*]");

            Map<String,String> links = new TreeMap<String, String>();
            for (Element element : content) {
                links.put(element.text(), element.attr("href"));
                //System.out.println(String.format("%-20s %s",element.text(),element.attr("href")));
            }

            for (String s : links.keySet()) {
                System.out.println(String.format("%-32s -> %s",s,links.get(s)));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
