package de.hahn.apibrowser;

import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by post_000 on 25.12.2014.
 */
public class TestPage {

    public static final int EQUAL = 0;
    private Api api;
    private String path;

    @Before
    public void before() {
        String name = "java8";
        path = "D:\\Offline API\\java8\\api\\java";
        api = new Api(name, path);
    }

    @Test
    public void pageLesser() {
        Page page1 = new Page(Paths.get(path,"\\key1.html"), api);
        Page page2 = new Page(Paths.get(path,"\\key2.html"), api);

        assertThat(page1.compareTo(page2)).isLessThan(EQUAL);
    }

    @Test
    public void pageGreater() {
        Page page1 = new Page(Paths.get(path,"\\key2.html"), api);
        Page page2 = new Page(Paths.get(path,"\\key1.html"), api);

        assertThat(page1.compareTo(page2)).isGreaterThan(EQUAL);
    }

    @Test
    public void pageEqual() {
        Page page1 = new Page(Paths.get(path,"\\key1.html"), api);
        Page page2 = new Page(Paths.get(path,"\\key1.html"), api);

        assertThat(page1.compareTo(page2)).isEqualTo(EQUAL);
    }

    @Test
    public void testGetPage() {
        Api api = new Api("java8", "D:\\Programmieren\\Offline API\\java8\\api").load();
        Page page = api.findPage("string");
        assertThat(page).has(new Condition<Page>() {
            @Override
            public boolean matches(Page page) {
                return page.getName().equalsIgnoreCase("String");
            }
        });
    }
}
