package de.hahn.apibrowser;

import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by post_000 on 28.12.2014.
 */
public class TestFileScanner {
    
    @Test
    public void testFilesInFolder() {
        List<File> files = new ArrayList<>();
        assertThat(files).isNotEmpty();
    }

    @Test
    public void testPackages() {
        Api api = new Api("Java8","D:\\Programmieren\\Offline API\\java8\\api");
        api.load();

        for (String s : api.getPackages()) {
            System.out.println(s);
        }
    }
}
